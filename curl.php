<?php

// This script is written for Linux system
// You will need to install whois to use this script
// this can be done with the following:-
// sudo apt-get install whois -y

?>

<style type="text/css">
	table {
	    font-size: 10px;
	}
</style>

<?php

$urls = array(
		"http://yahoo.co.uk"
	);

echo "<table>";

echo "<td>URL</td><td>Status</td><td>End Point</td><td>Owner</td>";

foreach ($urls as $url) {

	echo "<tr>";

	$whois = str_replace("http://", "", $url);

	$company = shell_exec('whois '.$whois);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$a = curl_exec($ch); // $a will contain all headers
	$a = substr($a, 0, strpos($a, 'Date'));


	$rurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL

	if (strpos($url, ".com") !== false || strpos($url, ".careers") !== false || strpos($url, ".london") !== false || strpos($url, ".eu") !== false || strpos($url, ".net") !== false || strpos($url, ".org") !== false) {
		$company = substr($company, strpos($company, "Registrant Name: "));
		$company = substr($company, 0, strpos($company, 'Registrant Organization: '));
		$company = strstr($company, ':');
		$company = str_replace(": ", "", $company);
	} else {
		$company = substr($company, strpos($company, "Registrant:"));
		$company = substr($company, 0, strpos($company, 'Registrant type:'));
		$company = strstr($company, '        ');
		$company = str_replace("        ", "", $company);
	}

	if(strpos($rurl, $whois) !== false) { $color = "#fff"; } else { $color = "red"; }

	echo "<td><a href='".$url."' target='_blank' >".$url."</a></td><td>".$a."</td><td style='background-color:".$color.";''>".$rurl."</td><td>".$company."</td>";

	echo "</tr>";

}